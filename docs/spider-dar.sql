-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 12-Nov-2015 às 21:29
-- Versão do servidor: 5.5.44-0+deb8u1
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spider-dar`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `guides`
--

CREATE TABLE IF NOT EXISTS `guides` (
`id` int(11) NOT NULL,
  `diretrizes_definicao` text,
  `diretrizes_aplicacao` text,
  `problema_definicao` text,
  `problema_aplicacao` text,
  `criterios_definicao` text,
  `criterios_aplicacao` text,
  `alternativas_definicao` text,
  `alternativas_alternativas` text,
  `metodos_definicao` text,
  `metodos_aplicacao` text,
  `avaliacao_definicao` text,
  `avaliacao_aplicacao` text,
  `solucoes_definicao` text,
  `solucoes_aplicacao` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `guides`
--

INSERT INTO `guides` (`id`, `diretrizes_definicao`, `diretrizes_aplicacao`, `problema_definicao`, `problema_aplicacao`, `criterios_definicao`, `criterios_aplicacao`, `alternativas_definicao`, `alternativas_alternativas`, `metodos_definicao`, `metodos_aplicacao`, `avaliacao_definicao`, `avaliacao_aplicacao`, `solucoes_definicao`, `solucoes_aplicacao`, `created`, `modified`) VALUES
(1, 'Diretrizes Definicao', 'Diretrizes Aplicacao', 'Problema Definicao', 'Problema Aplicacao', 'Criterios Definicao', 'Criterios Aplicacao', 'Alternativas Definicao', 'Alternativas Alternativas', 'Metodos Definicao', 'Metodos Aplicacao', 'Avaliacao Definicao', 'Avaliacao Aplicacao', 'Solucoes Definicao', 'Solucoes Aplicacao', '2015-11-12 20:49:15', '2015-11-12 20:49:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `motivations`
--

CREATE TABLE IF NOT EXISTS `motivations` (
`id` int(11) NOT NULL,
  `proposito` text NOT NULL,
  `planejamento` text NOT NULL,
  `contexto` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `motivations`
--

INSERT INTO `motivations` (`id`, `proposito`, `planejamento`, `contexto`, `created`, `modified`) VALUES
(1, 'Proposito', 'Planejamento', 'Contexto', '2015-11-12 23:17:03', '2015-11-12 23:19:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `motivations_users`
--

CREATE TABLE IF NOT EXISTS `motivations_users` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `motivation_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `motivations_users`
--

INSERT INTO `motivations_users` (`id`, `user_id`, `motivation_id`, `created`, `modified`) VALUES
(1, 1, 1, '2015-11-12 23:17:03', '2015-11-12 23:17:03'),
(2, 2, 1, '2015-11-12 23:19:25', '2015-11-12 23:19:25'),
(3, 3, 1, '2015-11-12 23:19:25', '2015-11-12 23:19:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `perfil` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `nome`, `perfil`, `created`, `modified`) VALUES
(1, 'Jadir Junior', 'Gerente', '2015-11-12 23:12:56', '2015-11-12 23:19:36'),
(2, 'Luiz Danin', 'Programador', '2015-11-12 23:18:30', '2015-11-12 23:18:30'),
(3, 'Bleno Silva', 'Programador', '2015-11-12 23:18:43', '2015-11-12 23:18:43'),
(4, 'Gessica', 'Programador', '2015-11-12 23:19:02', '2015-11-12 23:19:02'),
(5, 'Iuri Raiol', 'Programador', '2015-11-12 23:19:14', '2015-11-12 23:19:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guides`
--
ALTER TABLE `guides`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motivations`
--
ALTER TABLE `motivations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motivations_users`
--
ALTER TABLE `motivations_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guides`
--
ALTER TABLE `guides`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `motivations`
--
ALTER TABLE `motivations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `motivations_users`
--
ALTER TABLE `motivations_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
