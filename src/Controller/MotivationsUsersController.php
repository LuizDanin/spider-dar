<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MotivationsUsers Controller
 *
 * @property \App\Model\Table\MotivationsUsersTable $MotivationsUsers
 */
class MotivationsUsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Motivations']
        ];
        $this->set('motivationsUsers', $this->paginate($this->MotivationsUsers));
        $this->set('_serialize', ['motivationsUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Motivations User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $motivationsUser = $this->MotivationsUsers->get($id, [
            'contain' => ['Users', 'Motivations']
        ]);
        $this->set('motivationsUser', $motivationsUser);
        $this->set('_serialize', ['motivationsUser']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $motivationsUser = $this->MotivationsUsers->newEntity();
        if ($this->request->is('post')) {
            $motivationsUser = $this->MotivationsUsers->patchEntity($motivationsUser, $this->request->data);
            if ($this->MotivationsUsers->save($motivationsUser)) {
                $this->Flash->success(__('The motivations user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The motivations user could not be saved. Please, try again.'));
            }
        }
        $users = $this->MotivationsUsers->Users->find('list', ['limit' => 200]);
        $motivations = $this->MotivationsUsers->Motivations->find('list', ['limit' => 200]);
        $this->set(compact('motivationsUser', 'users', 'motivations'));
        $this->set('_serialize', ['motivationsUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Motivations User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $motivationsUser = $this->MotivationsUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $motivationsUser = $this->MotivationsUsers->patchEntity($motivationsUser, $this->request->data);
            if ($this->MotivationsUsers->save($motivationsUser)) {
                $this->Flash->success(__('The motivations user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The motivations user could not be saved. Please, try again.'));
            }
        }
        $users = $this->MotivationsUsers->Users->find('list', ['limit' => 200]);
        $motivations = $this->MotivationsUsers->Motivations->find('list', ['limit' => 200]);
        $this->set(compact('motivationsUser', 'users', 'motivations'));
        $this->set('_serialize', ['motivationsUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Motivations User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $motivationsUser = $this->MotivationsUsers->get($id);
        if ($this->MotivationsUsers->delete($motivationsUser)) {
            $this->Flash->success(__('The motivations user has been deleted.'));
        } else {
            $this->Flash->error(__('The motivations user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
