<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Guides Controller
 *
 * @property \App\Model\Table\GuidesTable $Guides
 */
class GuidesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('guides', $this->paginate($this->Guides));
        $this->set('_serialize', ['guides']);
    }

    /**
     * View method
     *
     * @param string|null $id Guide id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $guide = $this->Guides->get($id, [
            'contain' => []
        ]);
        $this->set('guide', $guide);
        $this->set('_serialize', ['guide']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $guide = $this->Guides->newEntity();
        if ($this->request->is('post')) {
            $guide = $this->Guides->patchEntity($guide, $this->request->data);
            if ($this->Guides->save($guide)) {
                $this->Flash->success(__('The guide has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The guide could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('guide'));
        $this->set('_serialize', ['guide']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Guide id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $guide = $this->Guides->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $guide = $this->Guides->patchEntity($guide, $this->request->data);
            if ($this->Guides->save($guide)) {
                $this->Flash->success(__('The guide has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The guide could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('guide'));
        $this->set('_serialize', ['guide']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Guide id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $guide = $this->Guides->get($id);
        if ($this->Guides->delete($guide)) {
            $this->Flash->success(__('The guide has been deleted.'));
        } else {
            $this->Flash->error(__('The guide could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
