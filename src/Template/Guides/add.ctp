<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações'); ?></li>
        <li><?= $this->Html->link(__('Listar Guia de Gestão de Decisão'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="guides form large-9 medium-8 columns content">
    <?= $this->Form->create($guide) ?>
        <h3><?= __('Adicionar Guia da Gestão da Decisão') ?></h3>
        <hr>
        <fieldset>
        	<legend>Estabelecer Diretrizes para Análise e Decisão Informação</legend>
        	<?php echo $this->Form->input('diretrizes_definicao',array('label' => 'Definição:')); ?>
        	<?php echo $this->Form->input('diretrizes_aplicacao',array('label' => 'Aplicação:')); ?>
        </fieldset>
        <fieldset>
        	<legend>O problema ou questão a ser objeto de um processo formal de tomada de decisão é definido</legend>
        	<?php echo $this->Form->input('problema_definicao',array('label' => 'Definição:')); ?>
        	<?php echo $this->Form->input('problema_aplicacao',array('label' => 'Aplicação:')); ?>
        </fieldset>
        <fieldset>
        	<legend>Estabelecer Critérios de Avaliação</legend>
        <?php echo $this->Form->input('criterios_definicao',array('label' => 'Definição:')); ?>
        <?php echo $this->Form->input('criterios_aplicacao',array('label' => 'Aplicação:')); ?>
        </fieldset>
        <fieldset>
        	<legend>Identificar Soluções Alternativas</legend>
        <?php echo $this->Form->input('alternativas_definicao',array('label' => 'Definição:')); ?>
        <?php echo $this->Form->input('alternativas_alternativas',array('label' => 'Aplicação:')); ?>
        </fieldset>
        <fieldset>
        	<legend>Selecionar Métodos de Avaliação</legend>
        <?php echo $this->Form->input('metodos_definicao',array('label' => 'Definição:')); ?>
        <?php echo $this->Form->input('metodos_aplicacao',array('label' => 'Aplicação:')); ?>
        </fieldset>
        <fieldset>
        	<legend>Avaliar Alternativas</legend>
        <?php echo $this->Form->input('avaliacao_definicao',array('label' => 'Definição:')); ?>
        <?php echo $this->Form->input('avaliacao_aplicacao',array('label' => 'Aplicação:')); ?>
        </fieldset>
        <fieldset>
        	<legend>Selecionar Soluções</legend>
        <?php echo $this->Form->input('solucoes_definicao',array('label' => 'Definição:')); ?>
        <?php echo $this->Form->input('solucoes_aplicacao',array('label' => 'Aplicação:')); ?>
        </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>
