<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Novo Guia de Gestão'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="guides index large-9 medium-8 columns content">
    <h3><?= __('Guias de Gestão de Decisão') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Id') ?></th>
                <th><?= $this->Paginator->sort('Criado em') ?></th>
                <th><?= $this->Paginator->sort('Modificado em') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($guides as $guide): ?>
            <tr>
                <td><?= $this->Number->format($guide->id) ?></td>
                <td><?= h($guide->created) ?></td>
                <td><?= h($guide->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $guide->id]) ?> | 
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $guide->id]) ?> |
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $guide->id], ['confirm' => __('Are you sure you want to delete # {0}?', $guide->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('<< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próximo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
