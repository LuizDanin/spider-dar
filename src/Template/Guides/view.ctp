<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar Guia'), ['action' => 'edit', $guide->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar Guia'), ['action' => 'delete', $guide->id], ['confirm' => __('Deseja deletar o Guia Nº {0}?', $guide->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Guias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo Guia de Gestão de Decisão'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="guides view large-9 medium-8 columns content">
    <h3>Guia de Gestão de Decisão Nº <?= h($guide->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($guide->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($guide->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($guide->modified) ?></td>
        </tr>
    </table>
    
    <fieldset>
    <legend>Estabelecer Diretrizes para Análise e Decisão Informação</legend>
    <div class="row">
        <h5><?= __('Diretrizes Definicao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->diretrizes_definicao)); ?>
    </div>
    <div class="row">
        <h5><?= __('Diretrizes Aplicacao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->diretrizes_aplicacao)); ?>
    </div>
    </fieldset>
    <fieldset>
    <legend>O problema ou questão a ser objeto de um processo formal de tomada de decisão é definido</legend>
    <div class="row">
        <h5><?= __('Problema Definicao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->problema_definicao)); ?>
    </div>
    <div class="row">
        <h5><?= __('Problema Aplicacao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->problema_aplicacao)); ?>
    </div>
    </fieldset>
    <fieldset>
    <legend>Estabelecer Critérios de Avaliação</legend>
    <div class="row">
        <h5><?= __('Criterios Definicao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->criterios_definicao)); ?>
    </div>
    <div class="row">
        <h5><?= __('Criterios Aplicacao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->criterios_aplicacao)); ?>
    </div>
    </fieldset>
    <fieldset>
    <legend>Identificar Soluções Alternativas</legend>
    <div class="row">
        <h5><?= __('Alternativas Definicao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->alternativas_definicao)); ?>
    </div>
    <div class="row">
        <h5><?= __('Alternativas Alternativas') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->alternativas_alternativas)); ?>
    </div>
    </fieldset>
    <fieldset>
    <legend>Selecionar Métodos de Avaliação</legend>
    <div class="row">
        <h5><?= __('Metodos Definicao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->metodos_definicao)); ?>
    </div>
    <div class="row">
        <h5><?= __('Metodos Aplicacao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->metodos_aplicacao)); ?>
    </div>
    </fieldset>
    <fieldset>
    <legend>Avaliar Alternativas</legend>
    <div class="row">
        <h5><?= __('Avaliacao Definicao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->avaliacao_definicao)); ?>
    </div>
    <div class="row">
        <h5><?= __('Avaliacao Aplicacao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->avaliacao_aplicacao)); ?>
    </div>
    </fieldset>
    <fieldset>
    <legend>Selecionar Soluções</legend>
    <div class="row">
        <h5><?= __('Solucoes Definicao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->solucoes_definicao)); ?>
    </div>
    <div class="row">
        <h5><?= __('Solucoes Aplicacao') ?></h5>
        <?= $this->Text->autoParagraph(h($guide->solucoes_aplicacao)); ?>
    </div>
    </fieldset>
</div>
