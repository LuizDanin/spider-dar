<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Motivation'), ['action' => 'edit', $motivation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Motivation'), ['action' => 'delete', $motivation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $motivation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Motivations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Motivation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="motivations view large-9 medium-8 columns content">
    <h3><?= h($motivation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($motivation->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($motivation->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($motivation->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Proposito') ?></h4>
        <?= $this->Text->autoParagraph(h($motivation->proposito)); ?>
    </div>
    <div class="row">
        <h4><?= __('Planejamento') ?></h4>
        <?= $this->Text->autoParagraph(h($motivation->planejamento)); ?>
    </div>
    <div class="row">
        <h4><?= __('Contexto') ?></h4>
        <?= $this->Text->autoParagraph(h($motivation->contexto)); ?>
    </div>
    <div class="related">
        <h4><?= __('Participantes Envolvidos') ?></h4>
        <?php if (!empty($motivation->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Perfil') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($motivation->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->nome) ?></td>
                <td><?= h($users->perfil) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
