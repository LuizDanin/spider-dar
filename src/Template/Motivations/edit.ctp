<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $motivation->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $motivation->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Motivations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="motivations form large-9 medium-8 columns content">
    <?= $this->Form->create($motivation) ?>
    <fieldset>
        <legend><?= __('Edit Motivation') ?></legend>
        <?php
            echo $this->Form->input('proposito');
            echo $this->Form->input('planejamento');
            echo $this->Form->input('contexto');
            echo $this->Form->input('users._ids', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
