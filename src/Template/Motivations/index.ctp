<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Cadastrar Motivações e Objetivos'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="motivations index large-9 medium-8 columns content">
    <h3><?= __('Motivações e Objetivos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($motivations as $motivation): ?>
            <tr>
                <td><?= $this->Number->format($motivation->id) ?></td>
                <td><?= h($motivation->created) ?></td>
                <td><?= h($motivation->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $motivation->id]) ?> |
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $motivation->id]) ?> |
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $motivation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $motivation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
