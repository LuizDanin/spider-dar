<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Motivations User'), ['action' => 'edit', $motivationsUser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Motivations User'), ['action' => 'delete', $motivationsUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $motivationsUser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Motivations Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Motivations User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Motivations'), ['controller' => 'Motivations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Motivation'), ['controller' => 'Motivations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="motivationsUsers view large-9 medium-8 columns content">
    <h3><?= h($motivationsUser->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $motivationsUser->has('user') ? $this->Html->link($motivationsUser->user->id, ['controller' => 'Users', 'action' => 'view', $motivationsUser->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Motivation') ?></th>
            <td><?= $motivationsUser->has('motivation') ? $this->Html->link($motivationsUser->motivation->id, ['controller' => 'Motivations', 'action' => 'view', $motivationsUser->motivation->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($motivationsUser->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($motivationsUser->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($motivationsUser->modified) ?></td>
        </tr>
    </table>
</div>
