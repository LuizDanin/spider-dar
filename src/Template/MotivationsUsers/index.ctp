<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Motivations User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Motivations'), ['controller' => 'Motivations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Motivation'), ['controller' => 'Motivations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="motivationsUsers index large-9 medium-8 columns content">
    <h3><?= __('Motivations Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('motivation_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($motivationsUsers as $motivationsUser): ?>
            <tr>
                <td><?= $this->Number->format($motivationsUser->id) ?></td>
                <td><?= $motivationsUser->has('user') ? $this->Html->link($motivationsUser->user->id, ['controller' => 'Users', 'action' => 'view', $motivationsUser->user->id]) : '' ?></td>
                <td><?= $motivationsUser->has('motivation') ? $this->Html->link($motivationsUser->motivation->id, ['controller' => 'Motivations', 'action' => 'view', $motivationsUser->motivation->id]) : '' ?></td>
                <td><?= h($motivationsUser->created) ?></td>
                <td><?= h($motivationsUser->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $motivationsUser->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $motivationsUser->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $motivationsUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $motivationsUser->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
