<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Guide Entity.
 *
 * @property int $id
 * @property string $diretrizes_definicao
 * @property string $diretrizes_aplicacao
 * @property string $problema_definicao
 * @property string $problema_aplicacao
 * @property string $criterios_definicao
 * @property string $criterios_aplicacao
 * @property string $alternativas_definicao
 * @property string $alternativas_alternativas
 * @property string $metodos_definicao
 * @property string $metodos_aplicacao
 * @property string $avaliacao_definicao
 * @property string $avaliacao_aplicacao
 * @property string $solucoes_definicao
 * @property string $solucoes_aplicacao
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Guide extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
