<?php
namespace App\Model\Table;

use App\Model\Entity\Motivation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Motivations Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class MotivationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('motivations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Users', [
            'foreignKey' => 'motivation_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'motivations_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('proposito', 'create')
            ->notEmpty('proposito');

        $validator
            ->requirePresence('planejamento', 'create')
            ->notEmpty('planejamento');

        $validator
            ->requirePresence('contexto', 'create')
            ->notEmpty('contexto');

        return $validator;
    }
}
