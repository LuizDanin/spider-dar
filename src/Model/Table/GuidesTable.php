<?php
namespace App\Model\Table;

use App\Model\Entity\Guide;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Guides Model
 *
 */
class GuidesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('guides');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('diretrizes_definicao');

        $validator
            ->allowEmpty('diretrizes_aplicacao');

        $validator
            ->allowEmpty('problema_definicao');

        $validator
            ->allowEmpty('problema_aplicacao');

        $validator
            ->allowEmpty('criterios_definicao');

        $validator
            ->allowEmpty('criterios_aplicacao');

        $validator
            ->allowEmpty('alternativas_definicao');

        $validator
            ->allowEmpty('alternativas_alternativas');

        $validator
            ->allowEmpty('metodos_definicao');

        $validator
            ->allowEmpty('metodos_aplicacao');

        $validator
            ->allowEmpty('avaliacao_definicao');

        $validator
            ->allowEmpty('avaliacao_aplicacao');

        $validator
            ->allowEmpty('solucoes_definicao');

        $validator
            ->allowEmpty('solucoes_aplicacao');

        return $validator;
    }
}
